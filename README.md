# Treudiners

![Emosido engañado (El Jueves 2171)](emosidoengañado.png)

Tú, fiel de la banca ética, eres de Triodos.

Y es que, por lo mucho que te guste, a la hora de sacar pasta, no dejas de ponerle los cuernos. Es que sus cajeros son tan escasos...

Y aquí es donde lo bueno sale caro: las demás entidades te cobran una comisión por el privilegio de usar los suyos. Como hay una Caixa en cada calle, si no estás atento, acabas pagando bonito.

¿Acaso ser ético tiene que costar tanto?

Es decir, ¿puedes sacar dinero sin que te cobren?

¡Claro que sí, consultando Treudiners!

Recopilamos los cajeros que te permiten sacar dinero gratis.

Usa la lista de cajeros para encontrar el que más te convenga.


## Lista de cajeros

La lista de cajeros es un [fichero de texto](cajeros.txt) en este mismo repo.

Los datos se han recopilado a mano de las webs de las mismas entidiades el día 2019-03-06.

De momento cubrimos la ciudad de Barcelona, y tenemos dos entidades que colaboran con Triodos.

Iremos añadiendo los cajeros de más entidiades y en más localidades.

Si falta tu localidad y quieres ayudarnos a mejorar, ¡mándanos una PR!


## Historias

Forjado en la Ciudad Condal.

El autor no es nada más que un cliente de Triodos harto de tragar las comisiones de cajeros.

En catalán, «treu diners» significa «saca dinero».
