"Puedes retirar dinero en efectivo de forma gratuita en los cajeros de Triodos Bank.

Además, puedes realizar en total 3 extracciones gratuitas al mes en la red de cajeros del Grupo Banco Popular, ING, Bankinter, Bankia, Cajamar, Banca March y Cashzone. A partir de la 3ª extracción y también en el resto de cajeros, se te repercutirá el importe de la comisión establecida por cada entidad. Puedes ver la comisión en la pantalla del cajero antes de finalizar la operación."

https://www.triodos.es/es/cuentas-y-tarjetas/tarjeta-debito

* Triodos
* Grupo Banco Popular
* ING
* Bankinter
* Bankia
* Cajamar
* Banca March
* Cashzone

Triodos
https://www.triodos.es/es/nuestras-oficinas/

Avda. Diagonal, 418. Casa de Les Punxes
Barcelona 08037
Barcelona

Banco Popular
https://www.bancopopular.es/personas/canales/sucursales-y-cajeros

Sucursal 0081
AV. GRAN VIA LES CORTS CATALANES 408
8015, BARCELONA

Sucursal 0228
VILA VILA, 101-103 (PARALELO)
08004, 08004 BARCELONA

Sucursal 0135
RAMBLA DELS ESTUDIS, 128 
8002, BARCELONA

Sucursal 0128
VIA LAIETANA, 33 
8003, BARCELONA

Sucursal 0205
COMTE D'URGELL 125 
8036, BARCELONA

Sucursal 1468
CASANOVA 153 
8036, BARCELONA

Sucursal 1588
PASEO DE GRACIA 54
08007, BARCELONA

Sucursal 6002
PZ. FRANCESC MACIA 3 
8021, BARCELONA

Sucursal 0122
TUSET 20-24
8006, BARCELONA

Sucursal 0082
CORCEGA 325
8037, BARCELONA

Sucursal 0227
VIA AUGUSTA 158
8006, BARCELONA

Sucursal 0592
BALMES 372-378 
8022, BARCELONA

Sucursal 1334
CERDEÑA 525
8024, BARCELONA

Sucursal 0303
CAPITAN ARENAS 21
8034, BARCELONA

Sucursal 0084
COMTES DEL BELL LLOC 144 
8014, BARCELONA

Sucursal 0289
AV. DE MADRID, 33
8028, BARCELONA

Sucursal 1816
AV. MASNOU 55, B 
8905, L'HOSPITALET DE LLOBREGAT

Sucursal 0247
BARCELONA 102
8901, L'HOSPITALET DE LLOBREGAT

Sucursal 1314
PZ. EUROPA 1-3 
8908, L'HOSPITALET DE LLOBREGAT

Sucursal 1134
MARE DE DEU DE PORT 248-250 
8038, BARCELONA

Sucursal 0553
G V LES CORTS CATALANES 715 
8013, BARCELONA

Sucursal 0287
MALLORCA 419 
8013, BARCELONA

Sucursal 1276
AVDA. ICARIA, 148
8005, BARCELONA

Sucursal 0136
PERE IV 141-149
8018, BARCELONA

Sucursal 0129
AV. MERIDIANA, 143-145 
8026, BARCELONA

Sucursal 0812
PS. MARAGALL 255 
8032, BARCELONA

Sucursal 1338
PS. FABRA Y PUIG 171 
8031, BARCELONA

Sucursal 0310
PS. VALLDAURA 212
8042, BARCELONA

Sucursal 1346
PO. DE MARAGALL, 362
08031, 08031 BARCELONA

Sucursal 1494
JOSEP PLA 25-27
8019, BARCELONA

Sucursal 0264
GUIPUZCOA 148-150
8020, BARCELONA

Sucursal 0323
P. DE TORRAS I BAGES, 51 
8030, BARCELONA

ING
https://www.ing.es/contactar/buscar-oficinas-ing/

Avenida Diagonal 475, 08036 Barcelona

Avenida Meridiana 216,08027 Barcelona

Calle Numancia 63, 08029 Barcelona

Centro comercial La Maquinista
Carrer de Potosí 2 | 08030 | Barcelona

Cajero de ING
Polígono Industrial Salinas, Carrer Alberedes 6-12 | 08830 | Barcelona

Nationale-Nederlanden
Carrer de Roger de Llúria 118 | 08037 | Barcelona

Centro comercial Magic
Carrer de la Concòrdia 1

Centro comercial Montigalá
Passeig Olof Palme 28-36

Money Exchange Paralelo
Carrer de Mata 1

Centro Comercial Parc Valles
Avenida Textil 1

Centro Comercial Alcampo Sant Quirze
Sant Quirze del Vallès